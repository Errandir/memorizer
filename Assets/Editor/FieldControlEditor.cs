using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(FieldControl))]
public class FieldControlEditor : Editor
{

    public override void OnInspectorGUI()
    {
        /*GameManager gameManager = (GameManager)target;
        gameManager.NewGridSizeX=EditorGUILayout.IntField("Test X",gameManager.NewGridSizeX);
        gameManager.NewGridSizeY = EditorGUILayout.IntField("Test Y", gameManager.NewGridSizeY);
        EditorGUILayout.LabelField("Size",(gameManager.NewGridSizeX*gameManager.NewGridSizeY).ToString());*/
        //EditorGUILayout.HelpBox("This is a help box", MessageType.Info);
        //EditorGUILayout.TextField("Text field");
        DrawDefaultInspector();
        FieldControl fieldControl = (FieldControl)target;
        if (GUILayout.Button("Create field"))
        {
            fieldControl.CreateNewGrid(fieldControl.NewGridSizeX,fieldControl.NewGridSizeY);
        }
        if (GUILayout.Button("Move field"))
        {
            fieldControl.MoveGrid(fieldControl.NewGridPositionX, fieldControl.NewGridPositionY);
        }

    }
}
