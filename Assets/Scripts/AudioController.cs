using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController:MonoBehaviour
{
    [HideInInspector]public AudioSource backgroundAudioSource, mainAudioSource;
    public AudioClip BackGroundMusic, ButtonClick, SelectCard, FailSound, SuccessSound, BonusActivationSound;

    void Awake()
    {
        AudioSource[] sources = gameObject.GetComponents<AudioSource>();
        backgroundAudioSource = sources[0];
        mainAudioSource = sources[1];
        backgroundAudioSource.playOnAwake = true;
        backgroundAudioSource.loop = true;
        mainAudioSource.playOnAwake = false;
    }

    void Start()
    {
        PlaySound(backgroundAudioSource, BackGroundMusic);
    }
    public void ChangeVolume(AudioSource source, float volume)
    {
        source.volume = volume;
    }

    public void ChangeMainVolume(float volume)
    {
        mainAudioSource.volume = volume;
        PlaySound(mainAudioSource,ButtonClick);
    }
    public void ChangeBackVolume(float volume)
    {
        backgroundAudioSource.volume = volume;
    }
    public void PlaySound(AudioSource source, AudioClip clip)
    {
        source.clip = clip;
        source.Play();
    }

    public void PlayButtonClick()
    {
        mainAudioSource.clip = ButtonClick;
        mainAudioSource.Play();
    }
    public void PlayBonusClick()
    {
        mainAudioSource.clip = BonusActivationSound;
        mainAudioSource.Play();
    }
}
