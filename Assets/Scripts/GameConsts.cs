using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConsts
{
    public const string GridParentObject = "GridParent";
    public const string FaceCardGO = "Face";
    public const string BackCardGO = "Back";
    public const string MaskCardGO = "Mask";
    public const string ScriptableObjectsFolder = "ScriptableObjects";
    public const string PrefabsFolder = "Prefabs";
    public const string BackgroundTag = "Background";
    //public const string MainCameraTag = "MainCamera";
    public const string MainAudioPlayerPrefs = "MainAudioValue";
    public const string BackgroundAudioPlayerPrefs = "BackGroundAudioValue";
    public const string MainSceneName = "Main";
}
