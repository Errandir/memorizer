using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIControl:MonoBehaviour
{
    public float RotationDuration;
    public static UIControl Instance;
    public static Camera MainCamera;
    public float SwipeTime;
    public GameObject MainMenuCanvas, SettingsMenuCanvas,UICanvas,FinalMenuCanvas;

    public Slider MainAudioSlider,BackGroundAudioSlider;
    public Button BonusButton;
    [Header("Texts")] public TextMeshProUGUI CardCountText;
    public TextMeshProUGUI GuessedCountText;
    public TextMeshProUGUI MovesCountText;
    public TextMeshProUGUI GuessedCountFinalText;
    public TextMeshProUGUI MovesCountFinalText;
    public TextMeshProUGUI PercentageText;
    [SerializeField]private TextMeshProUGUI LevelText;

    private void Awake()
    {
        if (UIControl.Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        //Camera=GameObject.FindWithTag(GameConsts.MainCameraTag);
        MainCamera = Camera.main;
        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(MainCamera);
        Observer.LevelChanged += OnLevelChanged;
        Observer.GameOver += ShowFinalMenu;
        Observer.BonusQuantitySet += OnBonusQuantitySet;
        Observer.BonusQuantityChanged += OnBonusQuantityChanged;
        Observer.MovesQuantityChanged += OnMovesQuantityChanged;
        Observer.GuessedCardsCountChanged += OnGuessedCardsCountChanged;
        Observer.TotalCardsCountChanged += OnTotalCardsCountChanged;
        Observer.NewGridCreated += OnNewGridCreated;
    }

    private void OnNewGridCreated(int cardsLeft, int stepsCount, int pairesCount)
    {
        CardCountText.text = cardsLeft.ToString();
        MovesCountText.text = stepsCount.ToString();
        GuessedCountText.text = pairesCount.ToString();
    }
    private void OnTotalCardsCountChanged(int quantity)
    {
        StartCoroutine(SingeUITextUpdate(CardCountText, quantity.ToString()));
    }
    private void OnGuessedCardsCountChanged(int quantity)
    {
        StartCoroutine(SingeUITextUpdate(GuessedCountText, quantity.ToString()));
    }
    private void OnMovesQuantityChanged(int quantity)
    {
        StartCoroutine(SingeUITextUpdate(MovesCountText, quantity.ToString()));
    }

    void OnLevelChanged(int newLevel)
    {
        LevelText.text = newLevel.ToString();
    }

    void OnBonusQuantitySet(int quantity)
    {
        BonusButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = quantity.ToString();
    }
    void OnBonusQuantityChanged(int quantity)
    {
        StartCoroutine(SingeUITextUpdate(BonusButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>(), quantity.ToString()));
    }
    //Update coroutine for UI object
    IEnumerator SingeUITextUpdate(TextMeshProUGUI textMeshProUgui, string value)
    {
        //Waiting until rotation finishes (launched in another coroutine)
        yield return new WaitForSeconds(RotationDuration * 2);
        textMeshProUgui.text = value;
        for (int i = 0; i < 20; i++)
        {
            float step = (float)i / 10;
            textMeshProUgui.fontSize += step;
            yield return new WaitForSeconds(0.01f);
        }
        for (int i = 0; i < 20; i++)
        {
            float step = (float)i / 10;
            textMeshProUgui.fontSize -= step;
            yield return new WaitForSeconds(0.01f);
        }
    }
    void Start()
    {
        MainAudioSlider.value = PlayerPrefs.GetFloat(GameConsts.MainAudioPlayerPrefs,0.5f);
        BackGroundAudioSlider.value = PlayerPrefs.GetFloat(GameConsts.BackgroundAudioPlayerPrefs, 0.5f);
        Button btn = BonusButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        //Accessing FiledControl of Gamemanager instance
        FieldControl fieldControl = GameManager.Instance.GetComponent<FieldControl>();
        //If there are bonuses on this level and if there are no selected and rotated cards on field
        if (fieldControl.BonusCount > 0 && fieldControl.ComparisonList.Count==0)
        {
            MainCamera.GetComponent<AudioController>().PlayBonusClick();
            //Showing all cards
            fieldControl.ShowAllCards();
            //Decreasing bonus count
            fieldControl.BonusCount--;
            //Updating bonus count text on the button
            fieldControl.UpdateBonusCount(fieldControl.BonusCount);
        }
    }
    public void StartTheGame()
    {
        Scene scene = SceneManager.GetActiveScene();
        PlayerPrefs.SetFloat(GameConsts.MainAudioPlayerPrefs, MainAudioSlider.value);
        PlayerPrefs.SetFloat(GameConsts.BackgroundAudioPlayerPrefs, BackGroundAudioSlider.value);
        gameObject.GetComponent<Image>().enabled = false;
        MainMenuCanvas.SetActive(false);
        FinalMenuCanvas.SetActive(false);
        SettingsMenuCanvas.SetActive(false);
        UICanvas.SetActive(true);
        MainCamera.transform.position=new Vector3(0f,0f,-10f);
        MainCamera.GetComponent<Camera>().orthographicSize = 40;
        if (scene.name != GameConsts.MainSceneName)
        {
            SceneManager.LoadScene(GameConsts.MainSceneName);
        }
        else
        {
            GameManager.Instance.StepsCount = 0;
            GameManager.Instance.PairesCount = 0;
            GameManager.Instance.Level = 0;
            GameManager.Instance.GetComponent<FieldControl>().ProcessNewLevel();
        }
    }

    private void ShowFinalMenu()
    {
        UICanvas.SetActive(false);
        FinalMenuCanvas.SetActive(true);
        GuessedCountFinalText.text =GameManager.Instance.PairesCount.ToString();
        MovesCountFinalText.text =GameManager.Instance.StepsCount.ToString();
        float percentage= ((float)GameManager.Instance.PairesCount / (float)GameManager.Instance.StepsCount)*100;
        if (float.IsNaN(percentage))
        {
            percentage = 0;
        }
        PercentageText.text = percentage.ToString("F2");
    }
    public void Exit()
    {
        Application.Quit();
        //For instant exit on mobile devices
        //System.Diagnostics.Process.GetCurrentProcess().Kill();
    }

    //Obsolete method
    public void ShowSettingsMenu()
    {
        if (!GameManager.Instance.IsSettingsMenuShown)
        {
            StartCoroutine(SwipeLeft());
        }
        else
        {
            StartCoroutine(SwipeRight());
        }
    }

    public void ShowSettingsMenuInstantly()
    {
        Scene scene = SceneManager.GetActiveScene();
        /*if (!IsSettingsMenuShown)
        {*/
            SettingsMenuCanvas.SetActive(!GameManager.Instance.IsSettingsMenuShown);
            GameManager.Instance.IsSettingsMenuShown = !GameManager.Instance.IsSettingsMenuShown;
        /*}
        else
        {
            SettingsMenuCanvas.SetActive(false);
            //UICanvas.SetActive(true);
            IsSettingsMenuShown = false;
        }*/
        if (scene.name == GameConsts.MainSceneName)
        {
            UICanvas.SetActive(!GameManager.Instance.IsSettingsMenuShown);
        }
    }

    //Obsolete swiping coroutine
        IEnumerator SwipeLeft()
    {
        RectTransform mainMenuRectTransform = MainMenuCanvas.GetComponent<RectTransform>();
        RectTransform settingsMenuRectTransform = SettingsMenuCanvas.GetComponent<RectTransform>();
        float step = SwipeTime / 100;
        for (int i = 0; i < 100; i++)
        {
            mainMenuRectTransform.position=new Vector3(mainMenuRectTransform.position.x-7f,mainMenuRectTransform.position.y,mainMenuRectTransform.position.z);
            settingsMenuRectTransform.position = new Vector3(settingsMenuRectTransform.position.x - 7f, settingsMenuRectTransform.position.y, settingsMenuRectTransform.position.z);
            yield return new WaitForSeconds(step);
        }
        GameManager.Instance.IsSettingsMenuShown = true;
    }

        //Obsolete swiping coroutine
    IEnumerator SwipeRight()
    {
        RectTransform mainMenuRectTransform = MainMenuCanvas.GetComponent<RectTransform>();
        RectTransform settingsMenuRectTransform = SettingsMenuCanvas.GetComponent<RectTransform>();
        float step = SwipeTime / 100;
        for (int i = 0; i < 100; i++)
        {
            mainMenuRectTransform.position = new Vector3(mainMenuRectTransform.position.x + 7f, mainMenuRectTransform.position.y, mainMenuRectTransform.position.z);
            settingsMenuRectTransform.position = new Vector3(settingsMenuRectTransform.position.x + 7f, settingsMenuRectTransform.position.y, settingsMenuRectTransform.position.z);
            yield return new WaitForSeconds(step);
        }
        GameManager.Instance.IsSettingsMenuShown = false;
    }
}
