using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardGrid
{
    private int width;
    private int height;
    private float cellSize;
    private Vector3 originPosition;
    private int[,] gridArray;
    private GameObject[,] GameObjectsArray;
    public CardGrid(int width, int height,float cellSize,Vector3 originPosition, List<GameObject> prefab, GameObject parent)
    {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        this.originPosition = originPosition;
        gridArray=new int[width,height];
        GameObjectsArray = new GameObject[width,height];
        //Getting list of card pairs
        List<int> pairedList = GetPairedList(gridArray.GetLength(0), gridArray.GetLength(1), prefab);
        int i=0;
        for (int x = 0; x < gridArray.GetLength(0); x++)
        {
            for (int y = 0; y < gridArray.GetLength(1); y++)
            {
                //Filling two-dimensional array with cards from paired list. Using Object.Instantiate instead of GameObject.Instantiate for recommended code clearance
                GameObjectsArray[x, y] = Object.Instantiate(prefab[pairedList[i]],
                    GetWorldPosition(x, y) + new Vector3(cellSize, cellSize) * 0.5f, Quaternion.identity, parent.transform);
                //Debug.DrawLine(GetWorldPosition(x,y),GetWorldPosition(x,y+1),Color.white,100f);
                //Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x+1, y), Color.white, 100f);
                i++;
            }
        }
        //Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width, height), Color.white, 100f);
        //Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width, height), Color.white, 100f);
    }

    private Vector3 GetWorldPosition(int x, int y)
    {
        return new Vector3(x,y)*cellSize+originPosition;
    }

    //Obsolete GO creation
    private GameObject CreateGameObject(Transform parent, Vector3 localPosition, Sprite sprite,string name)
    {
        GameObject gameObject = new GameObject("GO_"+name,typeof(SpriteRenderer));
        Transform transform = gameObject.transform;
        transform.localScale=new Vector3(9f,9f,0f);
        transform.SetParent(parent, false);
        transform.localPosition = localPosition;
        gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
        gameObject.GetComponent<SpriteRenderer>().sortingLayerName="Foreground";
        return gameObject;
    }
    //Creating paired list
    private List<int> GetPairedList(int x, int y, List<GameObject> prefabs)
    {
        //List should be width*height size
        int count = x * y;
        List<int> result=new List<int>(count);
        int random;
        for (int i = 0; i < count/2; i++)
        {
            //Selecting random card prefab and adding two copies of it
            random = Random.Range(0, prefabs.Count);
            result.Add(random);
            result.Add(random);
        }
        //Implementing the Fisher-Yates shuffle 
        ExtensionClass.Shuffle(result);
        return result;
    }
    private void GetXY(Vector3 worldPosition,out int x, out int y)
    {
        x = Mathf.FloorToInt((worldPosition-originPosition).x / cellSize);
        y = Mathf.FloorToInt((worldPosition-originPosition).y / cellSize);
    }
    public GameObject GetValue(int x, int y)
    {
        if (x >= 0 && y >= 0 && x < width && y < height)
        {
            return GameObjectsArray[x, y];
        }
        else
        {
            return null;
        }
    }

    public GameObject GetValue(Vector3 worldPosition)
    {
        int x, y;
        GetXY(worldPosition,out x,out y);
        return GetValue(x, y);
    }
}
