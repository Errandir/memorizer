using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//using Microsoft.Unity.VisualStudio.Editor;
using TMPro;
using UnityEngine;
using Object = UnityEngine.Object;

public class FieldControl : MonoBehaviour
{
    private AudioController audioController;
    public float RotationDuration,FadeDuration;
    private CardGrid cardGrid;
    //List for comparing two selected cards. Making it public because we need to track if it is empty to click bonus button
    [SerializeField] public List<GameObject> ComparisonList=new List<GameObject>();
    //List of all cards in current level
    [SerializeField] private List<GameObject> cardList;
    //Cards left in current level
    private int cardsLeft;
    public int BonusCount;
    //List of all card prefabs
    private List<GameObject> prefabList=new List<GameObject>();
    private List<Level_SO> soList = new List<Level_SO>();
    //Counting active coroutines
    [SerializeField] private int coroutineCount = 0;
    //Arrays for getting all objects in a specific folder
    private Object[] so, prefabs;
    //Parent gameobject
    private GameObject parentGameObject;
    private bool levelFinished=false;
    //For testing purposes
    [HideInInspector]public int NewGridSizeX, NewGridSizeY, NewGridPositionX, NewGridPositionY;
    //Background gameobject
    private GameObject backGround;
    public float TimeToWaitBeforeRevert;
    private void Start()
    {
        audioController = Camera.main.GetComponent<AudioController>();
        //Loading all scriptable objects with level settings
        so = Resources.LoadAll(GameConsts.ScriptableObjectsFolder,typeof(ScriptableObject));
        //Loading all card prefabs
        prefabs = Resources.LoadAll(GameConsts.PrefabsFolder, typeof(GameObject));
        //Finding background gameobject to be able to move it later
        backGround = GameObject.FindWithTag(GameConsts.BackgroundTag);
        //Transferring from array to list just for convenience
        prefabList = prefabs.Cast<GameObject>().ToList();
        soList = so.Cast<Level_SO>().ToList();
        //Checking if level scriptableobjects are correct
        for (int i = soList.Count - 1; i >= 0; i--)
        {
            if (!IfLevelSOCorrect(soList[i]))
            {
                soList.Remove(soList[i]);
            }
        }
        //Creating parent gameobject for storing cards
        parentGameObject = new GameObject(GameConsts.GridParentObject);
        //Resetting its origin
        parentGameObject.transform.position=new Vector3(0,0,0);
        //Processing new level
        ProcessNewLevel();
    }

    //Checking if level SO is correct
    private bool IfLevelSOCorrect(Level_SO levelSo)
    {
        //Checking if the values are positive
        if (levelSo.X<0||levelSo.Y<0||levelSo.Bonuses<0)
        {
            return false;
        }
        //Checking if X and Y values are not more than 12 and 6 (for screen size)
        if (levelSo.X > 12 || levelSo.Y >6)
        {
            return false;
        }
        //Checking if the total cards count is even
        int check = levelSo.X * levelSo.Y;
        if (check % 2 != 0)
        {
            return false;
        }
        //If everything is OK
        return true;
    }
    
    //Preliminary checks and new grid creation
    public void ProcessNewLevel()
    {
        //Levels are created from scriptableObjects so we need to check if there is next level available
        if (GameManager.Instance.Level >= soList.Count)
        {
            //Assuming that it is the end of the game
            Observer.OnGameOver();
            //UIControl.Instance.ShowFinalMenu();
            levelFinished = false;
        }
        else
        {
            //Accessing level scriptable object according to current level
            Level_SO levelSo = (Level_SO)soList[GameManager.Instance.Level];
            //UIControl.Instance.LevelText.GetComponent<TextMeshProUGUI>().text=(GameManager.Instance.Level+1).ToString();
            //Calling event
            Observer.OnLevelChanged(GameManager.Instance.Level + 1);
            BonusCount = levelSo.Bonuses;
            //Setting bonus count text to UI button
            //UIControl.Instance.BonusButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text =BonusCount.ToString();
            Observer.OnBonusQuantitySet(BonusCount);
            //Launching grid creation
            CreateNewGrid(levelSo);
        }
    }
    private void Update()
    {
        //Checking LMB Press
        if (Input.GetMouseButtonDown(0))
        {
            //If there are no running coroutines (rotating or fading cards)
            if (coroutineCount==0&&!GameManager.Instance.IsSettingsMenuShown)
            {
                //Checking if there is a card where the user clicked
                GameObject v = cardGrid.GetValue(GetMouseWorldPosition());
                if (v != null)
                {
                    //Playing sound for card selection
                    audioController.PlaySound(audioController.mainAudioSource, audioController.SelectCard);
                    //Rotating card to show its face
                    StartCoroutine(Rotate(v));
                    //If this card is not already in comparison list
                    if (!ComparisonList.Contains(v)) 
                    { 
                        //And since there are less than two cards in that list
                        if (ComparisonList.Count < 2) 
                        { 
                            //Adding selected card to comparison list
                            ComparisonList.Add(v);
                        }
                    }
                }
                //If the comparison list contains two cards
                if (ComparisonList.Count == 2)
                {
                    //Counting steps
                    GameManager.Instance.StepsCount += 1;
                    //Compairing their sprites (since I don't know what else can a compare to identify if two gameobjects are instantiated from one prefab. Definitely not their names)
                    if (ComparisonList[0].transform.Find(GameConsts.FaceCardGO).gameObject.GetComponent<SpriteRenderer>().sprite ==
                            ComparisonList[1].transform.Find(GameConsts.FaceCardGO).gameObject.GetComponent<SpriteRenderer>().sprite)
                        {
                            //And counting successfully paired cards
                            GameManager.Instance.PairesCount += 1;
                        //Launching coroutine to play success sound after rotation
                        StartCoroutine(PlaySound(audioController.SuccessSound));
                        foreach (var c in ComparisonList)
                            {
                                //Showing masks and destroying cards
                                StartCoroutine(FadeIn(c));
                            }
                            //Updating UI text
                            //StartCoroutine(SingeUITextUpdate(UIControl.Instance.GuessedCountText.GetComponent<TextMeshProUGUI>(), GameManager.Instance.PairesCount.ToString()));
                            Observer.OnGuessedCardsCountChanged(GameManager.Instance.PairesCount);
                            //StartCoroutine(SingeUITextUpdate(UIControl.Instance.CardCountText.GetComponent<TextMeshProUGUI>(), cardsLeft.ToString()));
                            Observer.OnTotalCardsCountChanged(cardsLeft);
                        }
                        //If cards in comparison list doesn't match
                        else
                        {
                            //Launching coroutine to play fail sound after rotation
                            StartCoroutine(PlaySound(audioController.FailSound));
                            foreach (var c in ComparisonList)
                                //Rotating them back
                                StartCoroutine(RevertRotate(c));
                        }
                    //Starting coroutine for text changing and effects
                    //StartCoroutine(SingeUITextUpdate(UIControl.Instance.MovesCountText.GetComponent<TextMeshProUGUI>(), GameManager.Instance.StepsCount.ToString()));
                    Observer.OnMovesQuantityChanged(GameManager.Instance.StepsCount);
                    // Clearing comparison list
                    ComparisonList.Clear();
                }
            }
        }
        //If all cards are guessed and therefore level is finished
        if (levelFinished)
        {
            GameManager.Instance.Level++;
            //Processing new level
            ProcessNewLevel();
        }
    }

    //Coroutine to play sound after rotation
    IEnumerator PlaySound(AudioClip clip)
    {
        yield return new WaitForSeconds(RotationDuration*2);
        audioController.PlaySound(audioController.mainAudioSource, clip);
    }

    //Showing all cards when activating bonus
    public void ShowAllCards()
    {
        foreach (var card in cardList)
        {
            StartCoroutine(Rotate(card));
            StartCoroutine(RevertRotate(card, TimeToWaitBeforeRevert));
        }
    }

    //Updating bonus count to UI button
    public void UpdateBonusCount(int count)
    {
        Observer.OnBonusQuantityChanged(BonusCount);
        //StartCoroutine(SingeUITextUpdate(UIControl.Instance.BonusButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>(),BonusCount.ToString()));
    }

    //Level creation
    //[ContextMenu("Create new grid")]
    private void CreateNewGrid(Level_SO levelSo)
    {
        //Initialising card list
        cardList=new List<GameObject>();
        //Destroying child cards if it is not the first level
        for (int i = parentGameObject.transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(parentGameObject.transform.GetChild(i).gameObject);
        } 
        //Resetting parent transform
        parentGameObject.transform.position = new Vector3(0f, 0f, 0f);
        //Moving camera and background to the center of the grid
        MoveCameraAndBackGround(levelSo.X, levelSo.Y);
        //Counting cards in this level
        cardsLeft = levelSo.X * levelSo.Y;
        //Creating new level with parameters from that SO
        cardGrid=new CardGrid(levelSo.X, levelSo.Y, 10f, new Vector3(0, 0), prefabList,parentGameObject);
        //Getting all cards to the list to track 'em
        ListAllChilds();
        //Updating text for total cards count on this level
        Observer.OnNewGridCreated(cardsLeft, GameManager.Instance.StepsCount, GameManager.Instance.PairesCount);
        //UIControl.Instance.CardCountText.GetComponent<TextMeshProUGUI>().text = cardsLeft.ToString();
        //UIControl.Instance.MovesCountText.GetComponent<TextMeshProUGUI>().text = GameManager.Instance.StepsCount.ToString();
        //UIControl.Instance.GuessedCountText.GetComponent<TextMeshProUGUI>().text = GameManager.Instance.PairesCount.ToString();
        //Setting new level boolean to true
        levelFinished = false;
    }

    //For testing purposes - moving grid from custom inspector
    public void MoveGrid(int x,int y)
    {
        parentGameObject.transform.position=new Vector3(x,y,0);
    }

    //Moving camera and background to new center of the grid
    public void MoveCameraAndBackGround(int x,int y)
    {
        Vector3 newCameraPosition=new Vector3(x*5-5,y*5+5,-10f);
        Vector3 newBGPosition = new Vector3(x * 5-5, y * 5+5);
        Camera.main.transform.position = newCameraPosition;
        backGround.transform.position = newBGPosition;
    }

    //For testing purposes - creation from custom inspector
    public void CreateNewGrid(int x,int y)
    {
        for (int i = parentGameObject.transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(parentGameObject.transform.GetChild(i).gameObject);
        }
        parentGameObject.transform.position=new Vector3(0f,0f,0f);
        MoveCameraAndBackGround(NewGridSizeX,NewGridSizeY);
        cardList = new List<GameObject>();
            //Counting cards in this level
            cardsLeft = x * y;
            //Creating new level with parameters from that SO
            cardGrid = new CardGrid(x, y, 10f, new Vector3(0, 0), prefabList, parentGameObject);
            //Getting all cards to the list to track 'em
            ListAllChilds();
            //Updating text for total cards count on this level
            //StartCoroutine(SingeUITextUpdate(UIControl.Instance.CardCountText.GetComponent<TextMeshProUGUI>(), cardList.Count.ToString()));
            UIControl.Instance.CardCountText.GetComponent<TextMeshProUGUI>().text = cardsLeft.ToString();
            //Setting new level boolean to true
            levelFinished = false;
    }

    //Getting all child objects (cards in level) that are stored in parent gameobject
    private void ListAllChilds()
    {
        int childCount = parentGameObject.transform.childCount;
        for (int i = 0; i < childCount; i++)
        {
            cardList.Add(parentGameObject.transform.GetChild(i).gameObject);
        }
    }


    //Obsolete method
    /*IEnumerator UITextUpdate()
    {
        //Waiting until rotation finishes (launched in another coroutine)
        yield return new WaitForSeconds(RotationDuration * 2);
        //Updating text for total cards left on this level
        UIControl.Instance.CardCountText.GetComponent<TextMeshProUGUI>().text = cardsLeft.ToString();
        //Updating text for total steps count
        UIControl.Instance.MovesCountText.GetComponent<TextMeshProUGUI>().text = GameManager.Instance.StepsCount.ToString();
        //Updating text for total successfully paired cards count
        UIControl.Instance.GuessedCountText.GetComponent<TextMeshProUGUI>().text = GameManager.Instance.PairesCount.ToString();
        for (int i = 0; i < 20; i++)
        {
            float step = (float)i / 10;
            UIControl.Instance.MovesCountText.GetComponent<TextMeshProUGUI>().fontSize += step;
            UIControl.Instance.GuessedCountText.GetComponent<TextMeshProUGUI>().fontSize += step;
            UIControl.Instance.CardCountText.GetComponent<TextMeshProUGUI>().fontSize += step;
            yield return new WaitForSeconds(0.01f);
        }
        for (int i = 0; i < 20; i++)
        {
            float step = (float)i / 10;
            UIControl.Instance.MovesCountText.GetComponent<TextMeshProUGUI>().fontSize -= step;
            UIControl.Instance.GuessedCountText.GetComponent<TextMeshProUGUI>().fontSize -= step;
            UIControl.Instance.CardCountText.GetComponent<TextMeshProUGUI>().fontSize -= step;
            yield return new WaitForSeconds(0.01f);
        }
    }*/

    //Showing mask of success on a card
    IEnumerator FadeIn(GameObject gameObject)
    {
        coroutineCount++;
        cardsLeft--;
        //Waiting until rotation finishes (launched in another coroutine)
        yield return new WaitForSeconds(RotationDuration * 2);
        //Accessing spriterenderer of Mask child gameobject of a card
        SpriteRenderer maskSpriteRenderer = gameObject.transform.Find(GameConsts.MaskCardGO).gameObject.GetComponent<SpriteRenderer>();
        Color maskColor = maskSpriteRenderer.color;
        Color newColor;
        //Slowly changing alpha of the mask
        for (int i = 0; i < 10; i++)
        {
            float a = (float)i / 10;
            newColor = new Color(maskColor.r, maskColor.g, maskColor.b, a);
            maskSpriteRenderer.color = newColor;
            yield return new WaitForSeconds(0.01f);
        }
        yield return new WaitForSeconds(FadeDuration);
        //Getting hash of the gameobject
        int hash = gameObject.GetHashCode();
        //Checking all cards in the level
        foreach (var card in cardList)
        {
            //And comparing hash of the active card
            if (card.GetHashCode()==hash)
            {
                //When found, removing from active card list
                cardList.Remove(card);
                break;
            }
        }
        //And destroying active card
        Destroy(gameObject);
        //if there is no cards in the level no more
        if (cardsLeft == 0)
            //Level is finished
            levelFinished=true;
        coroutineCount--;
    }
    
    //Rotating selected card
    IEnumerator Rotate(GameObject gameObject)
    {
        coroutineCount++;
        //Accessing dotween controller on a card
        DoTweenController doTween = gameObject.GetComponent<DoTweenController>();
        //If not already rotated
        if (!doTween.Rotated)
        {
            //Rotating 90 degrees
            doTween.Rotate90(RotationDuration);
            yield return new WaitForSeconds(RotationDuration);
            //Activating face and deactivating back
            gameObject.transform.Find(GameConsts.FaceCardGO).gameObject.SetActive(!doTween.Rotated);//true
            gameObject.transform.Find(GameConsts.BackCardGO).gameObject.SetActive(doTween.Rotated);//false
            //Rotating back (imitating 180 degree turn)
            doTween.Rotate0(RotationDuration);
            //Marking card as rotated
            doTween.Rotated = true;
        }
        coroutineCount--;
    }

    //Reverting card rotation
    IEnumerator RevertRotate(GameObject gameObject)
    {
        coroutineCount++;
        //Waiting until rotation finishes (launched in another coroutine)
        yield return new WaitForSeconds(RotationDuration*2);
        //Accessing dotween controller on a card
        DoTweenController doTween = gameObject.GetComponent<DoTweenController>();
        //Rotating 90 degrees
        doTween.Rotate90(RotationDuration);
        yield return new WaitForSeconds(RotationDuration);
        //Activating back and deactivating face
        gameObject.transform.Find(GameConsts.FaceCardGO).gameObject.SetActive(!doTween.Rotated);//false
        gameObject.transform.Find(GameConsts.BackCardGO).gameObject.SetActive(doTween.Rotated);//true
        //Rotating back (imitating 180 degree turn)
        doTween.Rotate0(RotationDuration);
        //Marking card as not rotated
        doTween.Rotated = false;
        coroutineCount--;
    }

    IEnumerator RevertRotate(GameObject gameObject,float timeToWait)
    {
        coroutineCount++;
        yield return new WaitForSeconds(timeToWait);
        //Accessing dotween controller on a card
        DoTweenController doTween = gameObject.GetComponent<DoTweenController>();
        //Rotating 90 degrees
        doTween.Rotate90(RotationDuration);
        yield return new WaitForSeconds(RotationDuration);
        //Activating back and deactivating face
        gameObject.transform.Find(GameConsts.FaceCardGO).gameObject.SetActive(!doTween.Rotated);//false
        gameObject.transform.Find(GameConsts.BackCardGO).gameObject.SetActive(doTween.Rotated);//true
        //Rotating back (imitating 180 degree turn)
        doTween.Rotate0(RotationDuration);
        //Marking card as not rotated
        doTween.Rotated = false;
        coroutineCount--;
    }

    //Convert mouse world to game position
    private Vector3 GetMouseWorldPosition()
    {
        Vector3 vec = GetMouseWorldPositionWithZ(Input.mousePosition, Camera.main);
        vec.z = 0f;
        return vec;
    }

    private Vector3 GetMouseWorldPositionWithZ(Vector3 screenPosition, Camera worldCamera)
    {
        Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
        return worldPosition;
    }
}
