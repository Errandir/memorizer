using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    //Successfully guessed paires count
    public int PairesCount;
    //Steps count
    public int StepsCount;
    public int Level;
    public bool IsSettingsMenuShown = false;
    private void Awake()
    {
        if (GameManager.Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

}
