using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Level_SO")]
public class Level_SO : ScriptableObject
{
    public int X, Y;
    public int Bonuses;
}