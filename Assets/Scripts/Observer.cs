using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public delegate void NewGridCreatedDelegate(int c, int s, int p);
    public static event NewGridCreatedDelegate NewGridCreated;

    public static event Action<int> LevelChanged;
    public static event Action<int> BonusQuantitySet;
    public static event Action<int> BonusQuantityChanged;
    public static event Action<int> MovesQuantityChanged;
    public static event Action<int> GuessedCardsCountChanged;
    public static event Action<int> TotalCardsCountChanged;
    public static event Action GameOver;

    public static void OnNewGridCreated(int cardsLeft, int stepsCount, int pairesCount)
    {
        NewGridCreated?.Invoke(cardsLeft,stepsCount,pairesCount);
    }
    public static void OnLevelChanged(int newLevel)
    {
        LevelChanged?.Invoke(newLevel);
    }
    public static void OnGuessedCardsCountChanged(int quantity)
    {
        GuessedCardsCountChanged?.Invoke(quantity);
    }
    public static void OnTotalCardsCountChanged(int quantity)
    {
        TotalCardsCountChanged?.Invoke(quantity);
    }
    public static void OnBonusQuantitySet(int quantity)
    {
        BonusQuantitySet?.Invoke(quantity);
    }
    public static void OnBonusQuantityChanged(int quantity)
    {
        BonusQuantityChanged?.Invoke(quantity);
    }
    public static void OnMovesQuantityChanged(int quantity)
    {
        MovesQuantityChanged?.Invoke(quantity);
    }
    public static void OnGameOver()
    {
        GameOver?.Invoke();
    }
}
