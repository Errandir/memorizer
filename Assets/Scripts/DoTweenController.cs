using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoTweenController : MonoBehaviour
{
    public bool Rotated = false;
    private Transform Transform;
    void Start()
    {
        Transform = GetComponent<Transform>();
    }

    public void Shake(float duration, float strength,int  vibrato, float randomness )
    {
        Transform.DOShakePosition(duration,new Vector3(strength,strength,0f),vibrato, randomness, true,false);
        //Transform.DOShakeRotation(duration,strength,vibrato,randomness, true);
        Transform.DOShakeScale(duration,strength,vibrato,randomness, true);
    }
    public void RotateAndBack()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(Transform.DORotate(new Vector3(0f, 180f, 0f), 0.5f, RotateMode.Fast))
            .AppendInterval(0.5f)
            .Append(Transform.DORotate(new Vector3(0f, 360f, 0f), 0.5f, RotateMode.Fast));
    }
    public void Rotate()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(Transform.DORotate(new Vector3(0f, 180f, 0f), 0.5f, RotateMode.Fast));
    }
    public void Rotate90(float duration)
    {
        Transform.DORotate(new Vector3(0f, 90, 0f), duration, RotateMode.Fast);
    }
    public void Rotate0(float duration)
    {
        Transform.DORotate(new Vector3(0f, 0, 0f), duration, RotateMode.Fast);
    }
    public void RotateBack()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(Transform.DORotate(new Vector3(0f, 360f, 0f), 0.5f, RotateMode.Fast));
    }
}
